#include <string>

#include <QtWidgets>

#include "main.hpp"

int main() {
  std::string name {"qthelloworld"};
  char *argv[] {&name[0U], nullptr};
  auto argc = 1;
  QApplication a(argc, argv);
  QMainWindow w;
  // MainWidget w;
  w.show();
  return a.exec();
}
