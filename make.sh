#!/bin/sh

set -euo pipefail

CPP="g++ -std=c++14 -pedantic -Wall -Wextra -Werror -Wfatal-errors -Ofast"
LNK="g++"

II="\
-I/usr/include/qt \
-I/usr/include/qt/QtWidgets"

set -x

$CPP $II -c -o main.o main.cpp
$LNK -o qthelloworld main.o -lQt5Widgets -lQt5Gui -lQt5Core
